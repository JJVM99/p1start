package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	
	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node() {
			this.element = null;
			this.next = this.prev = null;

		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> comp;
	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		this.currentSize = 0;
		this.header = new Node<>();


		this.comp = comparator;
	}
	//Implemented the iterator
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		
		
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	//Edited to iterator to start at the indicated index
	private class CircularSortedDoublyLinkedListIndexIterator<E> implements Iterator<E>{
		
		private Node<E> nextNode;
		
		public CircularSortedDoublyLinkedListIndexIterator(int index) {
			this.nextNode =  (Node<E>) header.getNext();
			int count=0;
			while(count != index) {
				count++;
				this.nextNode = this.nextNode.getNext();
			}
		}
		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	//Edited the iterator to start at the end and move backwards
	private class CircularSortedDoublyLinkedListReverseIterator<E> implements ReverseIterator<E>{
		private Node<E> nextNode;
		
		
		public CircularSortedDoublyLinkedListReverseIterator() {
			this.nextNode = (Node<E>) header.getPrev();
		}

		public boolean hasPrevious() {
			return nextNode.getElement() != null;
		}
		
		public E previous() {
			if (this.hasPrevious()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getPrev();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	//Combined the tools of the index and reverse iterator
	private class CircularSortedDoublyLinkedListReverseIndexIterator<E> implements ReverseIterator<E>{
		private Node<E> nextNode;
		
		
		public CircularSortedDoublyLinkedListReverseIndexIterator(int index) {
			this.nextNode = (Node<E>) header.getPrev();
			int count=currentSize-1;
			while(count != index) {
				count--;
				this.nextNode = this.nextNode.getPrev();
			}
		}

		public boolean hasPrevious() {
			return nextNode.getElement() != null;
		}
		
		public E previous() {
			if (this.hasPrevious()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getPrev();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}

	@Override
	public Iterator iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}
	@Override
	public Iterator iterator(int index) {
		return new CircularSortedDoublyLinkedListIndexIterator<E>(index);
	}
	@Override
	public ReverseIterator<E> reverseIterator() {
		return new CircularSortedDoublyLinkedListReverseIterator<E>();
	}
	@Override
	public ReverseIterator<E> reverseIterator(int index) {
		return new CircularSortedDoublyLinkedListReverseIndexIterator<E>(index);
	}
	//add method (after checking if the object is not null and if the linked list is not empty) will loop through the list and uses the comparators
	// to verify the if they are greater than the values in the linked list, if the value is smaller it shall be placed in front of the value in the list
	// if its equal it shall be placed after the value in the list all equal copies shall be placed after the original value, if they are greater it will continue
	// if the value is the largest it will reach the end and it will be placed as the final value before the header
	@Override
	public boolean add(Object obj) {
		Node<E> result = new Node<>();
		result.setElement((E) obj);
		Node<E> temp = this.header.getNext();
		if(obj==null) {
			return false;
		}
		//if(temp.equals(this.header)){
			if(this.currentSize==0){
			this.header.setNext(result);
			result.setNext(this.header);
			result.setPrev(this.header);
			this.header.setPrev(result);
			this.currentSize++;
			return true;
		}else {
			while(!temp.equals(header)) {
				if(this.comp.compare(result.getElement(), temp.getElement()) < 0) {
					temp.getPrev().setNext(result);
					result.setPrev(temp.getPrev());
					temp.setPrev(result);
					result.setNext(temp);
					this.currentSize++;
					return true;
				} else if(this.comp.compare(result.getElement(), temp.getElement())==0) {
					temp.getNext().setPrev(result);
					result.setNext(temp.getNext());
					temp.setNext(result);
					result.setPrev(temp);
					this.currentSize++;
					return true;
				}else if((this.comp.compare(result.getElement(), temp.getElement())>0) && temp.getNext().equals(header)) {
					temp.setNext(result);
					result.setPrev(temp);
					result.setNext(header);
					header.setPrev(result);
					this.currentSize++;
					return true;
				}
				else {
					temp=temp.getNext();
				}
			}
		}
		return false;
	}
	@Override
	public int size() {

		return this.currentSize;
	}
	//Searches for the object in the parameter through the list and removes it if found following the process for linked lists and returns true if found and false if not
	@Override
	public boolean remove(Object obj) {
			Node<E> temp = this.header.getNext();
			if(this.isEmpty()) {
				return false;
			}
			while(temp !=this.header) {
				if(temp.getElement().equals(obj)) {
					temp.getPrev().setNext(temp.getNext());
					temp.getNext().setPrev(temp.getPrev());
					temp.setNext(null);
					temp.setPrev(null);
					temp.setElement(null);
					this.currentSize--;
					return true;
				}else {
					temp=temp.getNext();
				}
			}
			return false;
	}
	//follows the same process but i used a counter to reach the position that the index indicates, also i had to implement a method to check if the index value is within the parameters of the list size
	@Override
	public boolean remove(int index) {
		Node<E> temp = this.header.getNext();
		int count=0;
		if(this.isEmpty()) {
			return false;
		}
		if((index<0) || index>=this.currentSize) {
			throw new IndexOutOfBoundsException("Value exceeds limit");
		}else {
			
		while(temp !=this.header) {
			if(count==index) {
				temp.getPrev().setNext(temp.getNext());
				temp.getNext().setPrev(temp.getPrev());
				temp.setNext(null);
				temp.setPrev(null);
				temp.setElement(null);
				this.currentSize--;
				return true;
			}else {
				temp=temp.getNext();
				count++;
			}
		}
		}
		return false;
	}
	@Override
	public int removeAll(Object obj) {
		int result=0;
		while(this.remove(obj)) {
			
				result++;
		}
		return result;
	}

	@Override
	public E first() {
		return (this.isEmpty() ? null : this.header.getNext().getElement());
	}

	@Override
	public E last() {
		return (this.isEmpty() ? null : this.header.getPrev().getElement());
	}
	//searches and returns the the method in that particular index, wont return anything if index not present in the list
	@Override
	public E get(int index) {
		Node<E> temp= this.header.getNext();
		int result=0;
		while(temp!=this.header) {
			if(result==index) {
				return temp.getElement();
			}else {
				temp=temp.getNext();
				result++;
			}
		}
		return null;
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(this.header.getNext().getElement());
		}
	}

	@Override
	public boolean contains(Object e) {
		Node<E> temp = this.header.getNext();
		while(temp != this.header) {
			if(temp.getElement().equals(e)) {
				return true;
			}else {
				temp=temp.getNext();
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize==0;
	}
	//Done in a similar style to both remove method by searching for the object through a list and uses a counter to indicate the index where its found
	@Override
	public int firstIndex(Object e) {
		int result = 0;
		Node<E> search = this.header.getNext();
		while(search != this.header) {
			if(search.getElement().equals(e)) {
				return result;
			}else {
				search = search.getNext();
				result++;
			}
		}
		return -1;
	}
	//Reverse first index begins at the end and the counter will decrease instead of increasing
	@Override
	public int lastIndex(Object e) {
		int result = this.currentSize-1;
		Node<E> search = this.header.getPrev();
		while(search != this.header) {
			if(search.getElement().equals(e)) {
				return result;
			}else {
				search = search.getPrev();
				result--;
			}
		}
		return -1;
	}

}
