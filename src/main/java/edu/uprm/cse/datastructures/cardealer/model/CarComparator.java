package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{
//Decided to unite all the values as one individual string that had to be compared instead of comparing them individually
	@Override
	public int compare(Car x, Car y) {
		String temp1 = x.getCarBrand() + x.getCarModel() + x.getCarModelOption();
		String temp2 = y.getCarBrand() + y.getCarModel() + y.getCarModelOption();
		return temp1.compareTo(temp2);

	}
}
