package edu.uprm.cse.datastructures.cardealer.util;
//Implemented this interface so the reverse iterator could function for me and so i could use previous and has previous instead of next and has next
public interface ReverseIterator<E>{
	
	public boolean hasPrevious();
	public E previous();
}