package edu.uprm.cse.datastructures.cardealer.model;



import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
//Implements this class to be able to call upon the class and to reset the list if necessary
public class MockCarList {
	private static final CircularSortedDoublyLinkedList<Car> MockCarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());

	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return MockCarList;
	}

	public static void resetCars() {
		MockCarList.clear();
		
	}
}
