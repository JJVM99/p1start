package edu.uprm.cse.datastructures.cardealer;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.MockCarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class carDealer {
	//this list will be used every time this class needs to call upon the list with all the cars 
	private final CircularSortedDoublyLinkedList<Car> carList = MockCarList.getInstance(); 
	//Result will always be returned since if the list is empty result would still be empty and accurate
	//if not it will loop and return all the values in the carList
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] result = new Car[carList.size()]; 
		if(carList.isEmpty()) {
			return result;
		}
		
		for(int i=0; i<this.carList.size(); ++i) {
			result[i]=carList.get(i);
		}
		return result;
	}
	  @GET
	  @Path("/{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car getCar(@PathParam("id") long id) {
		  for(int i=0; i<carList.size(); ++i) {
			  if(this.carList.get(i).getCarId()==id) {
				  return this.carList.get(i);
			  }
		  }
		  throw new WebApplicationException(404);
	  }
	  //It will verify if the id is not present before adding the value
	  @POST
	    @Path("/add")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response addCar(Car cars){
		  for(int i=0; i<carList.size(); ++i) {
			  if(carList.get(i).getCarId()==cars.getCarId()) {
				  return Response.status(Response.Status.BAD_REQUEST).build();
			  }
		  }
	      carList.add(cars);
	      return Response.status(201).build();
	    }
	  //Will replace the one in the list by replacing it with the updated one which and proving it is the updated one by comparing the id 
	  @PUT
	    @Path("/{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateCar(@PathParam("id") long id, Car cars){
	     for(int i=0; i<this.carList.size(); ++i) {
	    	 if(this.carList.get(i).getCarId()==id) {
	    		 this.carList.remove(this.carList.get(i));
	    		 this.carList.add(cars);
	        return Response.status(Response.Status.OK).build();
	      } 
	    }
	     return Response.status(Response.Status.NOT_FOUND).build();
	  }
	  @DELETE
	    @Path("/{id}/delete")
	    public Response deleteCar(@PathParam("id") long id){
	     for(int i=0; i<carList.size(); ++i) {
	    	 if(this.carList.get(i).getCarId()==id) {
	    		 this.carList.remove(this.carList.get(i));
	    		 return Response.status(Response.Status.OK).build();
	    	 }
	     }
	     return Response.status(Response.Status.NOT_FOUND).build();
	    }
	 
}
